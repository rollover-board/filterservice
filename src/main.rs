mod model;
mod proto;

use futures_util::FutureExt;
use tokio::sync::oneshot;

use tonic::{transport::Server, Request, Response, Status};
use tonic_reflection::server::Builder;

use proto::filter::validate_service_server::{ValidateService, ValidateServiceServer};
use proto::filter::{ValidateRequest, ValidateResponse, ValidationAction};

use std::collections::HashMap;
use serde_derive::Deserialize;
use std::env;
use toml;


#[derive(Debug)]
pub struct Handler {
    spam: model::SpamModel,
}

impl Handler {
    pub fn new(model_path : &String) -> Handler {
        return Handler {
            spam: model::SpamModel::new(model_path).unwrap(),
        };
    }
}

#[tonic::async_trait]
impl ValidateService for Handler {
    async fn validate_post(
        &self,
        req: Request<ValidateRequest>,
    ) -> Result<Response<ValidateResponse>, Status> {
        let r = req.into_inner();
        let s = r.content;
        println!("content {}",&s);
        println!("title {}",r.title);
        let res = self.spam.test_string(s);
        println!("Spam Probability {} ", res*100f32);

        // TODO : PROCEED not rendering in response for some reason
        let mut action = ValidationAction::Proceed;
        
        if res > 0.85 {
            println!("Rejecting message with high spam probability");
            action = ValidationAction::Reject;
        }else {
            println!("Proceeding message as it's unlikely to be spam");
        }

        let resp = ValidateResponse {
            action: action as i32,
            id: r.id,
        };

        Ok(Response::new(resp))
    }
}


#[derive(Debug, Deserialize)]
struct Config {
    listen : HashMap<String, String>,
    model : HashMap<String, String>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Starting filterService");

    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];

    println!("Loading config from {}", file_path);

    let content = std::fs::read_to_string(file_path)?;
    let cfg: Config = toml::from_str(&content)?;

    let addr = cfg.listen["address"].parse()?;

    let model_path : String = cfg.model["directory"].parse()?;
    let handler = Handler::new(&model_path);

    let (_tx, rx) = oneshot::channel::<()>();
    let svc = ValidateServiceServer::new(handler);

    let reflection_service = Builder::configure()
        .register_encoded_file_descriptor_set(tonic::include_file_descriptor_set!("validation_file_reflection"))
        .build()
        .unwrap();

    println!("Starting SVC on {}", addr);
    Server::builder()
        .add_service(svc)
        .add_service(reflection_service)
        .serve_with_shutdown(addr, rx.map(drop))
        .await?;

    Ok(())
}
