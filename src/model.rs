use std::error::Error;
use std::path::Path;
use std::result::Result;
use tensorflow::Code;
use tensorflow::Graph;
use tensorflow::SavedModelBundle;
use tensorflow::SessionOptions;
use tensorflow::SessionRunArgs;
use tensorflow::Status;
use tensorflow::Tensor;

#[derive(Debug)]
pub struct SpamModel {
    model_bundle : SavedModelBundle,
    model_graph : Graph
}

impl SpamModel {

    pub fn new(model_dir : &String) -> Result<SpamModel, Box<dyn Error>> {
        println!("Loading model from {}", model_dir);
        if !Path::new(model_dir).exists() {
            return Err(Box::new(
                Status::new_set(
                    Code::NotFound,
                    &format!(
                        "Run 'prep' to download \
                     {} and try again.",
                        model_dir 
                    ),
                )
                .unwrap(),
            ));
        }

        // Load the saved model exported by regression_savedmodel.py.
        let mut graph = Graph::new();
        let bundle =
            SavedModelBundle::load(&SessionOptions::new(), &["serve"], &mut graph, model_dir)?;

        // prepare a tensor to feed the input
        let ret = SpamModel{
            model_bundle : bundle,
            model_graph : graph
        };

        Ok(ret)
    }

    pub fn test_string(&self, x: String) -> f32 {
        println!("Testing {}", x);
        let input_tensor = Tensor::<String>::new(&[1, 1]).with_values(&[x]).unwrap();

        let session = &self.model_bundle.session;
        let serv_sig = self.model_bundle.meta_graph_def().get_signature("serving_default").unwrap();

        // Grab input & output
        let input = serv_sig.get_input("input_1").unwrap();
        let in_op = self.model_graph.operation_by_name_required(&input.name().name).unwrap();

        let output = serv_sig.get_output("dense").unwrap();
        let out_op = self.model_graph.operation_by_name_required(&output.name().name).unwrap();

        // Run the tensor through the graph
        let mut args = SessionRunArgs::new();
        args.add_feed(&in_op, 0, &input_tensor);
        args.add_target(&out_op);
        let token = args.request_fetch(&out_op, 0);
        session.run(&mut args).unwrap();

        // Finally get output
        let outx: f32 = args.fetch(token).unwrap()[0];
		return outx;
    }
}
