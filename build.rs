use std::env;
use std::path::PathBuf;

fn main() -> Result<(), Box<dyn std::error::Error>> {

    let path = env::current_dir()?;
    let out_dir = "./src/proto";

    let reflection_descriptor =
        PathBuf::from(env::var("OUT_DIR").unwrap()).join("validation_file_reflection.bin");

    tonic_build::configure()
    .file_descriptor_set_path(&reflection_descriptor)
    .type_attribute(
        "ServerReflectionResponse.message_response",
        "#[allow(clippy::enum_variant_names)]",
    )
    .out_dir(out_dir)
    .compile(&["filter.proto"],&[path.join("rolloverproto")])?;
    Ok(())
}
