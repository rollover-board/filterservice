FROM rust:latest as builder
WORKDIR /usr/src/filterservice
COPY . .
RUN apt-get update && apt-get install -y protobuf-compiler curl && rm -rf /var/lib/apt/lists/*
RUN cargo install --path .

FROM tensorflow/tensorflow:2.10.0

RUN curl -kO https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-linux-x86_64-2.9.1.tar.gz && tar -C /usr/local -xzf libtensorflow-cpu-linux-x86_64-2.9.1.tar.gz && ldconfig /usr/local/lib

ENTRYPOINT ["/usr/local/bin/filterservice", "/etc/filterservice/filterservice.toml"]
RUN ln -s /usr/local/lib/python3.8/dist-packages/tensorflow/libtensorflow_framework.so.2 /lib/x86_64-linux-gnu/libtensorflow_framework.so.2
RUN mkdir -p /etc/filterservice/resources/filter-engine-model
COPY resources/filter-engine-model /etc/filterservice/resources/filter-engine-model
COPY --from=builder /usr/local/cargo/bin/filterservice /usr/local/bin/filterservice
COPY conf/filterservice-k8s.toml /etc/filterservice/filterservice.toml
